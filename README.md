[![pipeline status](https://gitlab.com/polyapp-open-source/E2E/badges/master/pipeline.svg)](https://gitlab.com/polyapp-open-source/E2E/-/commits/master)
[![go report](https://goreportcard.com/badge/gitlab.com/polyapp-open-source/E2E)](https://goreportcard.com/report/gitlab.com/polyapp-open-source/E2E)
[![license](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/polyapp-open-source/E2E/-/blob/master/LICENSE)


# E2E
Quickly get started with E2E tests and web page automation with this starter code.

Advanced behaviors like running every test across many emulated devices are hidden behind flags. Use `E2E --help` for more information.

## See it run
* `git clone https://gitlab.com/polyapp-open-source/E2E`
* Add $GOBIN to $PATH if it is not already done.
* Install the program `cd E2E` `go install`
* Learn about this program: `E2E --help`
* Run this program: `E2E http://localhost:4200/`

## Write your first test
Scenario: You're trying to write E2E tests to ensure your Progressive Web App is compliant.
One test you might write is to make sure the 'lang' attribute in the html tag is set.

First let's figure out what we're going to do. In this case, I think navigating to the web page,
waiting for the html element to appear, and then checking its 'lang' attribute is sufficient.

I went to the webpage and tried out commands in the console until I arrived at: `document.getElementsByTagName("html")[0].lang != ""`
I also want to wait for the web page to appear without using time.Sleep. chromedp.WaitVisible allows
waiting until a [CSS selector](https://www.w3schools.com/cssref/css_selectors.asp) exists on the page; waiting until "html" exists seems appropriate here.
* Edit `tests.go`
* Remove the failing test
* Create a new test called `E2ETestLangAttribute` 
* Copy over the code of the succeeding test
* There should be a syntax error in getTests(). Replace the old test with the new one in the list.
* Replace the chromedp.Evaluate eval parameter with `document.getElementsByTagName("html")[0].lang != ""`
* Replace `chromedp.Sleep(time.Second * 2)` with `chromedp.WaitVisible("html")`
Here is my test code:
```
func E2ETestLangAttribute(testHelper *TestHelper) {
	langSet := false
	tasks := []chromedp.Action{
		chromedp.Navigate(testHelper.BaseURL),
		chromedp.WaitVisible("html"),
		chromedp.Evaluate(`document.getElementsByTagName("html")[0].lang != ""`, &langSet),
	}

	err := chromedp.Run(testHelper.ChromedpCtx, tasks...)
	if err != nil {
		testHelper.Error("error in Run: " + err.Error())
		return // in regular tests you can call t.Fatal but calling os.Exit() like that isn't implemented in E2E
	}

	if !langSet {
		testHelper.Error("lang attribute was not set in html")
	}
}
```
* `go build`
* `go install` (or run the next command with ./)
* `E2E https://gitlab.com/polyapp-open-source/E2E/`
And we see the test we edited passed:
```
gledr@LAPTOP-AH6IAHBO MINGW64 ~/e2eb/E2E (master)
$ E2E https://gitlab.com/polyapp-open-source/E2E/
    ok    main.E2ETestLangAttribute            4.687s
    ok    main.ExampleE2ETestSucceeding        5.875s
PASS

gledr@LAPTOP-AH6IAHBO MINGW64 ~/e2eb/E2E (master)
```

## Writing More Tests
* Further examples are in the chromedp examples repo. See: https://github.com/chromedp/examples
* This is a starter repo. Please `rm -rf .git` or otherwise cannibalize this repo into your own project.

This repo will occasionally receive updates. To get those updates, click on the notification bell > 'Custom' > and check 'New Release'.
When a new release comes in, you can run the following commands to merge this repo's updates into your own:
```
git remote add -f E2E https://gitlab.com/polyapp-open-source/E2E
git merge E2E/master -v --allow-unrelated-histories
```
Resolve your merge conflicts. Then finish off with a commit:
```
git commit -m "Merged updates from gitlab.com/polyapp-open-source/E2E"
```
