// Command E2E is a command-line tool to run end to end (E2E) tests. Run `E2E -help` for more information.
package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/device"
	"log"
	"os"
	"reflect"
	"runtime"
	"text/tabwriter"
	"time"
)

// main is a command-line tool to run end to end (E2E) tests. Run `E2E -help` for more information.
func main() {
	logger := log.New(os.Stdout, "", 0)

	// all of the flags handled by E2E are defined here flagSet.Parse
	help := flag.Bool("help", false, "Set to print help text")
	parallel := flag.Int64("p", 10, ">0 to use parallelism. 0 to run tests sequentially.")
	emulateMany := flag.Bool("e", false, "Run every test more than once across a set of emulated devices. The devices are representative of different screen sizes. (default false)")
	emulateManyLong := flag.Bool("emulate-many", false, "Long name version of option -e (default false)")
	verbose := flag.Bool("v", false, "enable verbose debugging for all tests. (default false)")

	chromeFlags := GetChromeFlags()
	flag.Parse()

	if *help {
		printHelp(logger)
		return
	}
	args := flag.Args()
	if len(args) != 1 {
		fmt.Println("BaseURL parameter is required and must be the only parameter. For help run: 'E2E --help'")
		os.Exit(1)
	}
	baseURL := args[0]
	if len(baseURL) < 1 || baseURL[len(baseURL)-1] != '/' {
		fmt.Println("BaseURL parameter invalid. Must end in /")
		os.Exit(1)
	}

	if *emulateManyLong {
		*emulateMany = true
	}
	var emulateDevices []chromedp.Device
	if *emulateMany {
		emulateDevices = []chromedp.Device{
			device.Reset,
			device.IPhone4,
			device.Pixel2XL,
			device.KindleFireHDX,
			device.IPad,
			device.IPadPro,
		}
	} else {
		emulateDevices = []chromedp.Device{
			device.Reset,
		}
	}

	tests := getTests()
	var testHelpers []*TestHelper
	if *parallel > 0 {
		testHelpers = runTestsParallel(tests, baseURL, chromeFlags, *verbose, emulateDevices, logger)
	} else {
		testHelpers = runTests(tests, baseURL, chromeFlags, *verbose, emulateDevices, logger)
	}
	exitCode := logResults(logger, testHelpers)
	os.Exit(exitCode)
}

// printHelp prints all help text except for parameter help text because that help text is very long.
func printHelp(logger *log.Logger) {
	logger.Print(`'E2E' operates nothing like 'go test' and you should not expect any of its features.
Gotchas: E2E's test durations are NOT reproducible, especially if run in parallel.
There's a 60 second (minimum) test timeout you can't adjust.

E2E prints a summary of the test results in the format:

	ok    E2ETestPageLoad     iPhone 4                        0.123s
	FAIL  E2ETestAnimation                                    1.234s
	ok    E2ETestReload       Blank implies Default Device    2.345s
	...
followed by detailed output for each failed test.

Usage:
E2E [Base URL] [options]

Parameters:
Base URL [required] the webpage being tested. Must contain a trailing slash.

-help [optional] print this help text. Does not require passing Base URL. (default false)

-p [optional] >0 to use parallelism. 0 to run tests sequentially. Behavior may eventually change to support a worker pool of size -p=X. (default 10)

-e [optional] When set, every test is run more than once where each test run is on a different emulated Device in the same browser with different tabs. Respects -p (parallelism) option. (default false)
--emulate-many

-v [optional] enable verbose debugging for all tests. This will log all browser activity in enabled domains before printing test results. (default false)

In addition to E2E parameters, several Chrome command line options are accepted.
Many command line options are volatile but some are stable and necessary. List: https://peter.sh/experiments/chromium-command-line-switches/
If the value is a string, it will be passed to Chrome as --name=value. If it's a boolean and true it will be
passed as --name and if boolean and false the value is either not passed or is unset if it was set by Chromedp.

Basic Example:
E2E https://localhost:4200/

Get a list of Chrome Browser Options by passing in an invalid flag:
E2E -invalid

Chrome Browser Options Example:
E2E -disable-background-networking=false https://localhost:4200/
`)
}

// runTests executes the tests sequentially.
// resulting TestHelpers len is len(tests)*len(emulatedDevices).
func runTests(tests []func(result *TestHelper), baseURL string, chromeFlags ChromeFlags, verbose bool, emulatedDevices []chromedp.Device, logger *log.Logger) []*TestHelper {
	testHelpers := make([]*TestHelper, len(tests)*len(emulatedDevices))
	for i, test := range tests {
		chromedpContext, cancelAllocCtx, cancelChromedpCtx, err := GetChromedpContext(chromeFlags, verbose, logger)
		if err != nil {
			logger.Fatalf("Error getting ChromedpCtx: %s", err.Error())
		}
		emulatedDeviceTestHelpers := runOneTestWithEmulatedDevices(test, baseURL, chromedpContext, emulatedDevices, false)
		for testHelperIndex := range emulatedDeviceTestHelpers {
			testHelpers[i*len(emulatedDevices)+testHelperIndex] = emulatedDeviceTestHelpers[testHelperIndex]
		}
		cancelAllocCtx()
		cancelChromedpCtx()
	}
	return testHelpers
}

// runTestsParallel executes the tests in parallel, with each test getting its own browser.
// resulting TestHelpers is len(tests)*len(emulatedDevices).
func runTestsParallel(tests []func(result *TestHelper), baseURL string, chromeFlags ChromeFlags, verbose bool, emulatedDevices []chromedp.Device, logger *log.Logger) []*TestHelper {
	var testHelpers []*TestHelper
	var testHelperChannels []chan *TestHelper
	testHelpers = make([]*TestHelper, len(tests)*len(emulatedDevices))
	testHelperChannels = make([]chan *TestHelper, len(tests)*len(emulatedDevices))
	for i, test := range tests {
		for testHelperIndex := range emulatedDevices {
			testHelperChannels[i*len(emulatedDevices)+testHelperIndex] = make(chan *TestHelper)
		}
		go func(testHelperChannels []chan *TestHelper, startIndex int, test func(result *TestHelper), baseURL string, chromeFlags ChromeFlags, verbose bool, emulatedDevices []chromedp.Device, logger *log.Logger) {
			chromedpContext, cancelAllocCtx, cancelChromedpCtx, err := GetChromedpContext(chromeFlags, verbose, logger)
			if err != nil {
				logger.Fatalf("Error getting ChromedpCtx: %s", err.Error())
			}
			// we need ranges of testHelperChannelsMux to accommodate all the data we're getting in parallel
			emulatedDeviceTestHelpers := runOneTestWithEmulatedDevices(test, baseURL, chromedpContext, emulatedDevices, true)
			for testHelperIndex := range emulatedDeviceTestHelpers {
				testHelperChannels[startIndex*len(emulatedDevices)+testHelperIndex] <- emulatedDeviceTestHelpers[testHelperIndex]
			}
			cancelAllocCtx()
			cancelChromedpCtx()
		}(testHelperChannels, i, test, baseURL, chromeFlags, verbose, emulatedDevices, logger)
	}
	for i := range testHelpers {
		testHelpers[i] = <-testHelperChannels[i]
	}
	return testHelpers
}

// runOneTestWithCtx runs a test after a browser context has been set up. There is no parallelism.
func runOneTestWithCtx(test func(result *TestHelper), baseURL string, chromedpContext context.Context, device chromedp.Device) (result *TestHelper) {
	result = &TestHelper{
		name:        runtime.FuncForPC(reflect.ValueOf(test).Pointer()).Name(), // ew
		success:     true,
		Device:      device,
		BaseURL:     baseURL,
		ChromedpCtx: chromedpContext,
	}
	tStart := time.Now()
	test(result)
	result.testLength = time.Since(tStart)
	return result
}

// runOneTestInNewTab runs a test in an existing browser as provided in chromedpContext and with a new tab.
func runOneTestInNewTab(test func(result *TestHelper), baseURL string, chromedpContext context.Context, device chromedp.Device) (result *TestHelper) {
	// the example doesn't cancel each tab individually. See: chromedp/chromedp/example_test.go
	tabContext, _ := chromedp.NewContext(chromedpContext)
	return runOneTestWithCtx(test, baseURL, tabContext, device)
}

// runOneTestWithEmulatedDevices takes a chromedpContext of a new browser and runs tests on all emulatedDevices in browser tabs.
func runOneTestWithEmulatedDevices(test func(result *TestHelper), baseURL string, chromedpContext context.Context, emulatedDevices []chromedp.Device, runInParallel bool) (results []*TestHelper) {
	results = make([]*TestHelper, len(emulatedDevices))
	resultsChannels := make([]chan *TestHelper, len(emulatedDevices))
	for i, aDevice := range emulatedDevices {
		resultsChannels[i] = make(chan *TestHelper)
		err := chromedp.Run(chromedpContext, chromedp.Emulate(aDevice))
		if err != nil {
			resultsChannels[i] <- &TestHelper{
				name:        runtime.FuncForPC(reflect.ValueOf(test).Pointer()).Name(), // ew
				testLength:  0,
				log:         []byte(err.Error()),
				success:     false,
				Device:      aDevice,
				BaseURL:     baseURL,
				ChromedpCtx: chromedpContext,
			}
			continue
		}
		if i == 0 {
			// first one runs in the original browser tab. This covers the case where -emulate-many is not passed in

			// set a test timeout here because it applies to all tabs in the browser
			// this helps compensate for uneven time allocations while running with --emulate-many in parallel
			chromedpContext, cancelFunc := context.WithTimeout(chromedpContext, time.Second*60*time.Duration(len(emulatedDevices)))
			defer cancelFunc()

			if runInParallel {
				go func(test func(result *TestHelper), baseURL string, chromedpContext context.Context, resultsChannel chan *TestHelper, device chromedp.Device) {
					resultsChannel <- runOneTestWithCtx(test, baseURL, chromedpContext, device)
				}(test, baseURL, chromedpContext, resultsChannels[i], aDevice)
			} else {
				// run test sequentially even if we're sending to the blocking channel in parallel
				result := runOneTestWithCtx(test, baseURL, chromedpContext, aDevice)
				go func(result *TestHelper, resultsChannel chan *TestHelper) {
					resultsChannel <- result
				}(result, resultsChannels[i])
			}
		} else {
			if runInParallel {
				go func(test func(result *TestHelper), baseURL string, chromedpContext context.Context, resultsChannel chan *TestHelper, device chromedp.Device) {
					resultsChannel <- runOneTestInNewTab(test, baseURL, chromedpContext, device)
				}(test, baseURL, chromedpContext, resultsChannels[i], aDevice)
			} else {
				// run test sequentially even if we're sending to the blocking channel in parallel
				result := runOneTestInNewTab(test, baseURL, chromedpContext, aDevice)
				go func(result *TestHelper, resultsChannel chan *TestHelper) {
					resultsChannel <- result
				}(result, resultsChannels[i])
			}
		}
	}
	for i := range emulatedDevices {
		results[i] = <-resultsChannels[i]
	}
	return results
}

// TestHelper has a subset of the functionality you would expect out of testing.T.
// It does not fully implement any common interfaces, and especially lacks 'FailNow' or similar behavior.
// It also holds all required test parameters and the Context you need to Run the test.
// NEVER read or write the private properties within a test.
type TestHelper struct {
	// name should NEVER be read/written in a test
	name string
	// testLength should NEVER be read/written in a test
	testLength time.Duration
	// log should NEVER be read/written in a test
	log []byte
	// success should NEVER be read/written in a test
	success bool
	// Device is set before the test is run. Device.Device().Name is printed in test results.
	Device chromedp.Device
	// BaseURL will be a string with an ending slash like https://localhost:4200/
	BaseURL string
	// ChromedpCtx is essential for running tests
	ChromedpCtx context.Context
}

// Write implements the io.Writer interface to help with writing to the log.
func (t *TestHelper) Write(p []byte) (n int, err error) {
	(*t).log = append((*t).log, p...)
	return len(p), nil
}

// Error is equivalent to Log followed by Fail.
func (t *TestHelper) Error(args ...interface{}) {
	t.Log(args...)
	t.Fail()
}

// Errorf is equivalent to Logf followed by Fail.
func (t *TestHelper) Errorf(format string, args ...interface{}) {
	t.Logf(format, args...)
	t.Fail()
}

// Fail marks the function as having failed but continues execution.
func (t *TestHelper) Fail() {
	t.success = false
}

// Failed returns true if the test failed.
func (t *TestHelper) Failed() bool {
	return !t.success
}

// Log formats its arguments using default formatting, analogous to Println, and records the text in the error log.
// For tests, the text will be printed only if the test fails.
func (t *TestHelper) Log(args ...interface{}) {
	_, file, line, _ := runtime.Caller(2)
	fmt.Fprintf(t, "\t%s:%d: ", file, line)
	fmt.Fprint(t, args...)
}

// Logf formats its arguments according to the format, analogous to Printf, and records the text in the error log.
// A final newline is added if not provided. For tests, the text will be printed only if the test fails.
func (t *TestHelper) Logf(format string, args ...interface{}) {
	_, file, line, _ := runtime.Caller(2)
	fmt.Fprintf(t, "\t%s:%d: ", file, line)
	fmt.Fprintf(t, format, args...)
}

// Name returns the name of the running test or benchmark.
func (t *TestHelper) Name() string {
	return t.name
}

// Time returns the amount of time the test took to run.
func (t *TestHelper) Time() time.Duration {
	return t.testLength
}

// logResults logs out all of the testHelpers as described in the -help text.
func logResults(logger *log.Logger, testHelpers []*TestHelper) (exitCode int) {
	atLeastOneFailed := false
	successFailWriter := tabwriter.NewWriter(logger.Writer(), 0, 0, 4, ' ', 0)
	for _, result := range testHelpers {
		if result.success {
			_, err := fmt.Fprintf(successFailWriter, "\tok\t%s\t%s\t%.3fs\n", result.Name(), result.Device.Device().Name, result.Time().Seconds())
			if err != nil {
				panic(err) // should never happen
			}
		} else {
			atLeastOneFailed = true
			_, err := fmt.Fprintf(successFailWriter, "\tFAIL\t%s\t%s\t%.3fs\n", result.Name(), result.Device.Device().Name, result.Time().Seconds())
			if err != nil {
				panic(err) // should never happen
			}
		}
	}
	err := successFailWriter.Flush()
	if err != nil {
		panic(err) // should never happen
	}
	if atLeastOneFailed {
		logger.Println("")
	}
	for _, result := range testHelpers {
		if result.Failed() {
			logger.Printf("--- FAIL: %s\n", result.Name())
			logger.Printf("    %s\n", result.log)
		}
	}
	if atLeastOneFailed {
		logger.Println("FAIL")
	} else {
		logger.Println("PASS")
	}
	if atLeastOneFailed {
		return 1
	}
	return 0
}
