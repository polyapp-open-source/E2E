package main

import (
	"bytes"
	"context"
	"flag"
	"github.com/chromedp/cdproto/inspector"
	cdpruntime "github.com/chromedp/cdproto/runtime"
	"github.com/chromedp/cdproto/target"
	"github.com/chromedp/chromedp"
	"github.com/mailru/easyjson/jlexer"
	"io/ioutil"
	"log"
	"reflect"
	"runtime"
	"strings"
	"sync"
	"unicode"
)

// GetChromedpContext prepares a chromedp headless client for interaction with the web server.
// logger is used if an error is thrown, something is logged to the console, etc.
// It will block until the web server has started. Caller should call defer cancelAllocCtx() and defer cancelChromedpCtx().
func GetChromedpContext(chromeFlags ChromeFlags, verbose bool, logger *log.Logger) (chromedpContext context.Context, cancelAllocCtx context.CancelFunc,
	cancelChromedpCtx context.CancelFunc, err error) {

	opts := CreateOpts(chromeFlags)

	// WSL needs the GPU disabled. See issue #10 on wasmbrowsertest
	if runtime.GOOS == "linux" && isWSL() {
		opts = append(opts,
			chromedp.DisableGPU,
		)
	}

	opts = append(opts, chromedp.Headless)

	var allocCtx context.Context
	allocCtx, cancelAllocCtx = chromedp.NewExecAllocator(context.Background(), opts...)
	var browserCtxOptions []chromedp.ContextOption
	if verbose {
		browserCtxOptions = []chromedp.ContextOption{
			chromedp.WithDebugf(logBrowser),
			chromedp.WithErrorf(logBrowser),
			chromedp.WithLogf(logBrowser),
		}
	}
	chromedpContext, cancelChromedpCtx = chromedp.NewContext(allocCtx, browserCtxOptions...)

	chromedp.ListenTarget(chromedpContext, func(ev interface{}) {
		handleEvent(chromedpContext, ev, logger)
	})

	return chromedpContext, cancelAllocCtx, cancelChromedpCtx, nil
}

// isWSL returns true if the OS is WSL, false otherwise.
// This method of checking for WSL has worked since mid 2016.
// https://github.com/microsoft/WSL/issues/423#issuecomment-328526847
func isWSL() bool {
	b, _ := ioutil.ReadFile("/proc/sys/kernel/osrelease")
	// if there was an error opening the file it must not be WSL, so ignore the error
	return bytes.Contains(b, []byte("Microsoft"))
}

// logBrowser is used to log browser events.
// The first argument is ignored. Two line breaks between messages improves readability.
func logBrowser(_ string, d ...interface{}) {
	for i := range d {
		switch t := d[i].(type) {
		case []byte:
			log.Printf("browser log: %v\n\n", string(d[i].([]byte)))
		case *jlexer.LexerError:
			// used in network logging
			log.Printf("browser log: %v\n\n", d[i].(*jlexer.LexerError).Error())
		default:
			log.Printf("browser logging error - type unhandled: %v\n\n", t)
		}
	}
}

// handleEvent responds to different events from the browser and takes appropriate action.
func handleEvent(ctx context.Context, ev interface{}, logger *log.Logger) {
	switch ev := ev.(type) {
	case *cdpruntime.EventConsoleAPICalled:
		// Print the full structure for transparency
		jsonBytes, err := ev.MarshalJSON()
		if err != nil {
			logger.Print(err)
		}
		if ev.Type == cdpruntime.APITypeError {
			// special case which can mean the WASM program never initialized
			logger.Printf("fatal error while trying to run tests: %v\n", string(jsonBytes))
		} else {
			logger.Printf("%v\n", string(jsonBytes))
		}
	case *cdpruntime.EventExceptionThrown:
		if ev.ExceptionDetails != nil && ev.ExceptionDetails.Exception != nil {
			logger.Printf("%s\n", ev.ExceptionDetails.Exception.Description)
		}
	case *target.EventTargetCrashed:
		logger.Printf("target crashed: status: %s, error code:%d\n", ev.Status, ev.ErrorCode)
		err := chromedp.Cancel(ctx)
		if err != nil {
			logger.Printf("error in cancelling context: %v\n", err)
		}
	case *inspector.EventDetached:
		logger.Println("inspector detached: ", ev.Reason)
		err := chromedp.Cancel(ctx)
		if err != nil {
			logger.Printf("error in cancelling context: %v\n", err)
		}
	}
}

// ChromeFlags are flags you can set in this program which are passed to chromedp when starting the browser.
// They include the set of default options passed to chromedp by chromedp/chromedp plus their named ExecAllocator options.
type ChromeFlags struct {
	NoFirstRun                          *bool
	DisableBackgroundNetworking         *bool
	EnableFeatures                      *string
	DisableBackgroundTimerThrottling    *bool
	DisableBackgroundingOccludedWindows *bool
	DisableBreakpad                     *bool
	DisableClientSidePhishingDetection  *bool
	DisableDefaultApps                  *bool
	DisableDevShmUsage                  *bool
	DisableExtensions                   *bool
	DisableFeatures                     *string
	DisableHangMonitor                  *bool
	UseMockKeychain                     *bool
	DisablePopupBlocking                *bool
	DisableIpcFloodingProtection        *bool
	DisablePromptOnRepost               *bool
	DisableRendererBackgrounding        *bool
	DisableSync                         *bool
	PasswordStore                       *string
	EnableAutomation                    *bool
	SafebrowsingDisableAutoUpdate       *bool
	MetricsRecordingOnly                *bool
	ForceColorProfile                   *string
	UserAgent                           *string
	ProxyServer                         *string
	WindowSize                          *string
	UserDataDir                         *string
	NoSandbox                           *bool
	DisableGPU                          *bool
	Headless                            *bool
	MuteAudio                           *bool
	HideScrollbars                      *bool
	NoDefaultBrowserCheck               *bool
	IgnoreCertificateErrors             *bool
	AllowInsecureLocalhost              *bool
}

// GetChromeFlags initializes all of the default Chrome flags used in E2E and returns references to them.
func GetChromeFlags() ChromeFlags {
	cFlags := ChromeFlags{}

	cFlags.UserDataDir = flag.String("user-data-dir", "",
		"manually set the profile directory used by Chrome")
	cFlags.ProxyServer = flag.String("proxy-server", "",
		"set the outbound proxy server")
	cFlags.WindowSize = flag.String("window-size", "",
		"width,height sets the initial window size")
	cFlags.UserAgent = flag.String("user-agent", "",
		"set the default User-Agent header")
	cFlags.NoSandbox = flag.Bool("no-sandbox", false,
		"disables the sandbox")
	cFlags.NoFirstRun = flag.Bool("no-first-run", true,
		"disables the first run dialog")
	cFlags.NoDefaultBrowserCheck = flag.Bool("no-default-browser-check", true,
		"disable the default browser check")
	cFlags.Headless = flag.Bool("headless", true,
		"run in headless mode")
	cFlags.HideScrollbars = flag.Bool("hide-scrollbars", true,
		"hide scrollbars - use with headless")
	cFlags.MuteAudio = flag.Bool("mute-audio", true,
		"mute audio - use with headless")
	cFlags.DisableGPU = flag.Bool("disable-gpu", false,
		"disable the GPU process")
	cFlags.DisableBackgroundNetworking = flag.Bool("disable-background-networking", true,
		"Disable several subsystems which run network requests in the background. This is for use when doing network performance testing to avoid noise in the measurements.")
	cFlags.EnableFeatures = flag.String("enable-features", "NetworkService,NetworkServiceInProcess",
		"Comma-separated list of feature names to enable.")
	cFlags.DisableBackgroundTimerThrottling = flag.Bool("disable-background-timer-throttling", true,
		"Disable task throttling of timer tasks from background pages.")
	cFlags.DisableBackgroundingOccludedWindows = flag.Bool("disable-backgrounding-occluded-windows", true,
		"Disable backgrounding renders for occluded windows. Done for tests to avoid nondeterministic behavior.")
	cFlags.DisableBreakpad = flag.Bool("disable-breakpad", true,
		"Disables the crash reporting.")
	cFlags.DisableClientSidePhishingDetection = flag.Bool("disable-client-side-phishing-detection", true,
		"Disables the client-side phishing detection feature. Note that even if client-side phishing detection is enabled, it will only be active if the user has opted in to UMA stats and SafeBrowsing is enabled in the preferences.")
	cFlags.DisableDefaultApps = flag.Bool("disable-default-apps", true,
		"Disables installation of default apps on first run. This is used during automated testing.")
	cFlags.DisableDevShmUsage = flag.Bool("disable-dev-shm-usage", true,
		"The /dev/shm partition is too small in certain VM environments, causing Chrome to fail or crash (see http://crbug.com/715363). Use this flag to work-around this issue (a temporary directory will always be used to create anonymous shared memory files).")
	cFlags.DisableExtensions = flag.Bool("disable-extensions", true,
		"Disable extensions.")
	cFlags.DisableFeatures = flag.String("disable-features", "site-per-process,TranslateUI,BlinkGenPropertyTrees",
		"Comma-separated list of feature names to disable. See also kEnableFeatures.")
	cFlags.DisableHangMonitor = flag.Bool("disable-hang-monitor", true,
		"Suppresses hang monitor dialogs in renderer processes. This may allow slow unload handlers on a page to prevent the tab from closing, but the Task Manager can be used to terminate the offending process in this case.")
	cFlags.DisableIpcFloodingProtection = flag.Bool("disable-ipc-flooding-protection", true,
		"Disables the IPC flooding protection. It is activated by default. Some javascript functions can be used to flood the browser process with IPC. This protection limits the rate at which they can be used.")
	cFlags.DisablePopupBlocking = flag.Bool("disable-popup-blocking", true,
		"Disable pop-up blocking.")
	cFlags.DisablePromptOnRepost = flag.Bool("disable-prompt-on-repost", true,
		"Normally when the user attempts to navigate to a page that was the result of a post we prompt to make sure they want to. This switch may be used to disable that check. This switch is used during automated testing.")
	cFlags.DisableRendererBackgrounding = flag.Bool("disable-renderer-backgrounding", true,
		"Prevent renderer process backgrounding when set.")
	cFlags.DisableSync = flag.Bool("disable-sync", true,
		"Disables syncing browser data to a Google Account.")
	cFlags.ForceColorProfile = flag.String("force-color-profile", "srgb",
		`Force all monitors to be treated as though they have the specified color profile. Accepted values are "srgb" and "generic-rgb" (currently used by Mac layout tests) and "color-spin-gamma24" (used by layout tests).`)
	cFlags.MetricsRecordingOnly = flag.Bool("metrics-recording-only", true,
		"Enables the recording of metrics reports but disables reporting. In contrast to kDisableMetrics, this executes all the code that a normal client would use for reporting, except the report is dropped rather than sent to the server. This is useful for finding issues in the metrics code during UI and performance tests.")
	cFlags.SafebrowsingDisableAutoUpdate = flag.Bool("safebrowsing-disable-auto-update", true,
		"Undocumented; may have been removed")
	cFlags.EnableAutomation = flag.Bool("enable-automation", true,
		"Enable indication that browser is controlled by automation.")
	cFlags.PasswordStore = flag.String("password-store", "basic",
		"Specifies which encryption storage backend to use. Possible values are kwallet, kwallet5, gnome, gnome-keyring, gnome-libsecret, basic. Any other value will lead to Chrome detecting the best backend automatically. TODO(crbug.com/571003): Once PasswordStore no longer uses the Keyring or KWallet for storing passwords, rename this flag to stop referencing passwords. Do not rename it sooner, though; developers and testers might rely on it keeping large amounts of testing passwords out of their Keyrings or KWallets.")
	cFlags.UseMockKeychain = flag.Bool("use-mock-keychain", true,
		"No description")
	cFlags.IgnoreCertificateErrors = flag.Bool("ignore-certificate-errors", true,
		"No longer documented. May have been removed.")
	cFlags.AllowInsecureLocalhost = flag.Bool("allow-insecure-localhost", true,
		"Enables TLS/SSL errors on localhost to be ignored (no interstitial, no blocking of requests)")
	return cFlags
}

var (
	optsMutex sync.Mutex
	optsCache []chromedp.ExecAllocatorOption
)

// CreateOpts looks through the command line flags and makes options for the browser.
func CreateOpts(chromeFlags ChromeFlags) []chromedp.ExecAllocatorOption {
	optsMutex.Lock()
	defer optsMutex.Unlock()
	if optsCache != nil {
		return optsCache
	}

	opts := make([]chromedp.ExecAllocatorOption, 0)

	// our goal is to place every field in ChromeFlags into an ExecAllocatorOption via
	// chromedp.Flag(flagName, flagValue)
	chromeFlagsValue := reflect.ValueOf(chromeFlags)
	chromeFlagsType := chromeFlagsValue.Type()
	for i := 0; i < chromeFlagsValue.NumField(); i++ {
		switch kind := chromeFlagsValue.Field(i).Interface().(type) {
		case *string:
			if kind == nil {
				// prevent nil pointer dereference
				continue
			}
			flagName := getFlagName(chromeFlagsType.Field(i).Name)
			flagValue := *(chromeFlagsValue.Field(i).Interface().(*string))
			if flagValue == "" {
				// this would be malformed
				continue
			}
			opts = append(opts, chromedp.Flag(flagName, flagValue))
		case *bool:
			if kind == nil {
				// prevent nil pointer dereference
				continue
			}
			flagName := getFlagName(chromeFlagsType.Field(i).Name)
			opts = append(opts, chromedp.Flag(flagName, *(chromeFlagsValue.Field(i).Interface().(*bool))))
		default:
			log.Print("Unhandled kind in CreateOpts")
			return nil
		}
	}

	optsCache = opts
	return opts
}

// getFlagName is how I enforce the flag naming convention. Flags named without the convention will be parsed
// by getFlagName incorrectly, which will pass those flags to chrome incorrectly, resulting in no behavior change.
func getFlagName(goName string) string {
	var goNameModified strings.Builder
	for i, r := range goName {
		if unicode.IsUpper(r) {
			if i != 0 {
				goNameModified.WriteRune('-')
			}
			goNameModified.WriteRune(unicode.ToLower(r))
		} else {
			goNameModified.WriteRune(r)
		}
	}
	return strings.ToLower(goNameModified.String())
}
