package main

import (
	"flag"
	"testing"
)

func TestGetChromeFlags(t *testing.T) {
	// should end up with quite a few flags
	chromeFlags := GetChromeFlags()
	if chromeFlags.DisableFeatures == nil || chromeFlags.NoSandbox == nil {
		t.Error("should have populated chromeFlags")
	}
}

func TestCreateOpts(t *testing.T) {
	chromeFlags := ChromeFlags{}
	opts := CreateOpts(chromeFlags)
	if len(opts) != 0 {
		t.Error("should not add any flags if ChromeFlags is empty")
	}

	trueVar := true
	chromeFlags.MuteAudio = &trueVar
	optsCache = nil
	opts = CreateOpts(chromeFlags)
	if len(opts) != 1 {
		t.Error("options can be values, they don't have to be from the flag package")
	}

	chromeFlags.MuteAudio = flag.Bool("someflag", true, "hi")
	optsCache = nil
	opts = CreateOpts(chromeFlags)
	if len(opts) != 1 {
		t.Error("if add one flag, should have one option")
	}

	chromeFlags.HideScrollbars = &trueVar
	chromeFlags.DisableFeatures = flag.String("someflag2", "false", "someflag2 usage")
	opts = CreateOpts(chromeFlags)
	if len(opts) != 1 {
		t.Error("You shouldn't be able to add flags after they are added the first time because of the cache")
	}

	optsCache = nil
	chromeFlags.ForceColorProfile = flag.String("klslds", "", "ksdlsdskdslksdlk")
	opts = CreateOpts(chromeFlags)
	if len(opts) != 3 {
		t.Error("Should have added a string flag and another bool, but not added the malformed string above")
	}
}

func TestGetFlagName(t *testing.T) {
	if "s-o-m-e" != getFlagName("sOME") {
		t.Error("sOME failed")
	}
	if "a-flag-li" != getFlagName("aFlagLi") {
		t.Error("aFlagLi failed")
	}
}
