package main

import (
	"github.com/chromedp/chromedp"
	"time"
)

// getTests is where you manually specify the tests this program will run.
func getTests() []func(testHelper *TestHelper) {
	tests := []func(testHelper *TestHelper){
		ExampleE2ETestFailing,
		ExampleE2ETestSucceeding,
	}
	return tests
}

// ExampleE2ETestFailing fails immediately after being called without using the provided Chrome browser.
func ExampleE2ETestFailing(testHelper *TestHelper) {
	testHelper.Error("This is a failing test")
}

// ExampleE2ETestSucceeding tests that the 'document' exists on the BaseURL webpage.
// Since even 404 pages have 'document' elements, this test should always pass.
func ExampleE2ETestSucceeding(testHelper *TestHelper) {
	documentExists := false
	tasks := []chromedp.Action{
		chromedp.Navigate(testHelper.BaseURL),
		chromedp.Sleep(time.Second * 2), // Wait a time for the 404 page to appear
		// chromedp.WaitVisible(`#doneButton`) // this style of waiting is better than 'Sleep' in most tests
		// Check that the text has been added to the DOM
		chromedp.Evaluate(`document != null`, &documentExists),
	}

	err := chromedp.Run(testHelper.ChromedpCtx, tasks...)
	if err != nil {
		testHelper.Error("error in Run: " + err.Error())
		return // in regular tests you can call t.Fatal but calling os.Exit() like that isn't implemented in E2E
	}

	if !documentExists {
		testHelper.Error("document should exist even if you're getting a 404")
	}
}
