module gitlab.com/polyapp-open-source/E2E

go 1.13

require (
	github.com/chromedp/cdproto v0.0.0-20200209033844-7e00b02ea7d2
	github.com/chromedp/chromedp v0.5.3
	github.com/mailru/easyjson v0.7.0
)
